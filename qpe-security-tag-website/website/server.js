const WebSocket = require('ws');
const express = require('express'); // Include ExpressJS
const app = express(); // Create an ExpressJS app
const bodyParser = require('body-parser'); // Middleware
const port = 3001 // Port we will listen on


var message = {"type":"tag_state","device_id":"000000000000","disabled":true};

const topicOfInterest1 = {
  action: "subscribe",
  data: {
    customer: "demo",
    story: 1,
    zone: "SecurityZone1"
  }
};

const topicOfInterest2 = {
  action: "subscribe",
  data: {
    customer: "demo",
    story: 1,
    zone: "SecurityZone2"
  }
};

const topicOfInterest3 = {
  action: "subscribe",
  data: {
    customer: "demo",
    story: 1,
    zone: "SecurityZone3"
  }
};

var tagline = "Kein Alarm anstehend";

var alarms = [

];

app.use(bodyParser.urlencoded({ extended: false }));
// app.use(express.static('src/www/static'));
//app.use(bodyParser.json())
app.set('view engine', 'ejs');


app.post('/refresh', (req, res) => {

  tagline = "Alarme gesamt: "+alarms.length;
  

   res.render('pages/alarm', {
     alarms: alarms,
     tagline: tagline
   }); 
});


app.post('/login', (req, res) => {
  // Insert Login Code Here
  let username = req.body.username;
  let password = req.body.password;

  console.log(username + ' '+ password);


  if(username === 'admin' && password === 'heringsdorf2021'){
    console.log('Someone successfully authenticated via Web-Interface');
    tagline = "Alarme gesamt: "+alarms.length;
    res.render('pages/alarm', {
      alarms: alarms,
      tagline: tagline      
    });
  } else {
    console.log('Warning: [Website]: Someone has tried to authenticated via Web-Interface');
    //res.send('Wrong login information!');
    res.render('pages/index', {
    });  
  }    
});

// use res.render to load up an ejs view file

// index page
app.get('/', function(req, res) {
  res.render('pages/index');
});

// Function to listen on the port
app.listen(port, "0.0.0.0", () => console.log(`This app is listening on port ${port}`));






// #############################################################################
// WEBSOCKET
var os = require("os");
var hostname = os.hostname();
console.log('Hostname:'+hostname);



var tags = []

async function connectToWebSocket() {
  //ws = new WebSocket('ws://PC-SR1:9898/');
  ws = new WebSocket('ws://digety-pe.local:80');
  ws.onopen = function() {
    console.log('WebSocket opened');
    ws.send(JSON.stringify(topicOfInterest1));
    ws.send(JSON.stringify(topicOfInterest2));
    ws.send(JSON.stringify(topicOfInterest3));
  };

  ws.onmessage = function(e) {
    //console.log('Message:', e.data);
    const obj = JSON.parse(e.data);    
    const time = new Date(); // aktuelles Datum und aktuelle Zeit

    if(tags.indexOf(obj.tag) === -1){
      console.log('New tag (' + obj.tag +") in Zone: " + obj.zones)
      tags.push(obj.tag)      
      alarms.push({ time: time.toLocaleString().substring(0,18), id: obj.tag, zones: obj.zones });
    } else{
      // TODO: Zeit und Zone in bestehendem JSON Objekt aktualisieren!
      //console.log('Bestehendes Tag (' + obj.tag +") in Zone: " + obj.zones)

      var results = [];
      var toSearch = obj.tag;
      
      for(var i=0; i<alarms.length; i++) {
        for(key in alarms[i]) {
          if(alarms[i][key].indexOf(toSearch)!=-1) {
            alarms[i] = { time: time.toLocaleString().substring(0,18), id: obj.tag, zones: obj.zones };

            // Not working :(
            // if(alarms[i].zones === obj.zones){
            //   console.log('Same Zone!')
            // } else {
            //   console.log('New Zone!')
            //   alarms[i] = { time: time.toLocaleString().substring(0,18), id: obj.tag, zones: obj.zones };
            // }
          }
        }
      }

    }   
  };

  ws.onclose = function(e) {
    console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
    setTimeout(function() {
      connectToWebSocket();
    }, 1000);
  };

  ws.onerror = function(err) {
    console.error('Socket encountered error: ', err.message, 'Closing socket');
    ws.close();
  };
}

connectToWebSocket();
