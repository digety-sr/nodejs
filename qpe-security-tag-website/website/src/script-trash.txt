    <!--<script src="websocket.js" type="module"></script>-->
    <script type="module">
      //import { AwsV4Signer } from "https://unpkg.com/aws4fetch@1.0.3/dist/aws4fetch.esm.js";

      const AWS_REGION = "eu-central-1"; // The region of your API-gateway
      const SOCKET_HOST = "fshr3lc4k9"; // Your API-gateway ID
      const ENV = "dev"; // The stage of your target deployment
      const REQ_PROTOCOL = "wss";
      const WEBSOCKET_URL = `${REQ_PROTOCOL}://${SOCKET_HOST}.execute-api.${AWS_REGION}.amazonaws.com`;
      console.log(WEBSOCKET_URL);

      const accessKeyId = "YOUR ACCESS KEY";
      const secretAccessKey = "YOUR SECRET ACCESS KEY";

      const opts = {
        url: WEBSOCKET_URL + "/" + ENV, // required, the AWS endpoint to sign
        accessKeyId: accessKeyId, // required, akin to AWS_ACCESS_KEY_ID
        secretAccessKey: secretAccessKey, // required, akin to AWS_SECRET_ACCESS_KEY
        signQuery: true
      };

      const topicOfInterest1 = {
        action: "subscribe",
        data: {
          customer: "demo",
          story: 1,
          zone: "SecurityZone1"
        }
      };

      const topicOfInterest2 = {
        action: "subscribe",
        data: {
          customer: "demo",
          story: 1,
          zone: "SecurityZone2"
        }
      };

      const topicOfInterest3 = {
        action: "subscribe",
        data: {
          customer: "demo",
          story: 1,
          zone: "SecurityZone3"
        }
      };

      //const signer = new AwsV4Signer(opts);
      function join(t, a, s) {
        function format(m) {
            let f = new Intl.DateTimeFormat('en', m);
            return f.format(t);
        }
        return a.map(format).join(s);
      }


      function addRow(tableID, obj) {
          // Beschaffe eine Referenz auf die Tabelle
          let tableRef = document.getElementById(tableID);

          // Füge am Ende der Tabelle eine neue Zeile an
          let newRow = tableRef.insertRow(-1);


          const heute = new Date(); // aktuelles Datum und aktuelle Zeit


          
          //document.getElementById("time1").innerHTML = heute.toISOString().substring(0,10);
          //document.getElementById("device1").innerHTML = obj.tag;
          //document.getElementById("zone1").innerHTML = obj.zones;

          // Erstelle in der Zeile eine Zelle am Index 0
          let newCell = newRow.insertCell(0);
          // Füge der Zelle einen textnode hinzu
          let newText = document.createTextNode(heute.toLocaleString().substring(0,18));

          newCell.appendChild(newText);

          let newCell1 = newRow.insertCell(1);
          // Füge der Zelle einen textnode hinzu
          let newText1 = document.createTextNode(obj.tag);
          newCell1.appendChild(newText1);

          let newCell2 = newRow.insertCell(2);
          // Füge der Zelle einen textnode hinzu
          let newText2 = document.createTextNode(obj.zones);
          newCell2.appendChild(newText2);

      }


      var tags = [];


      async function establishConnection() {
        //const { method, url, headers, body } = await signer.sign();
        //console.log(url);
        //var connection = new WebSocket(url.href);
        //var url = "ws://localhost:8080";
        var url = "ws://172.17.5.8";
        var connection = new WebSocket(url);
        // When the connection is open, send some data to the server
        connection.onopen = function() {
          //document.write("WebSocket connection established");
          //register for a certain topic
          connection.send(JSON.stringify(topicOfInterest1));
          connection.send(JSON.stringify(topicOfInterest2));
          connection.send(JSON.stringify(topicOfInterest3));
        };

        // Log errors
        connection.onerror = function(error) {
          document.write("<br>");
          document.write("WebSocket Error " + error);
        };

        // Log messages from the server
        connection.onmessage = function(e) {
          const obj = JSON.parse(e.data);
          //document.write("<br>");
          //document.write(e.data);
          //document.writeln('Gerät ('+obj.tag + ') in ' +obj.zones)
          

          if(tags.indexOf(obj.tag) === -1){
            console.log('Added ' + obj.tag)
            tags.push(obj.tag)
            addRow('my-table', obj);
          } else{
            console.log('Already exists!')
          }


          // const heute = new Date(); // aktuelles Datum und aktuelle Zeit
          // document.getElementById("time1").innerHTML = heute.toISOString().substring(0,10);
          // document.getElementById("device1").innerHTML = obj.tag;
          // document.getElementById("zone1").innerHTML = obj.zones;



        // Rufe addRow() mit der ID der Tabelle auf
        


          //console.log('Tag-ID: '+obj.tag + ' in Zone: ' +obj.zones);
        };
      }

      establishConnection();
    </script>