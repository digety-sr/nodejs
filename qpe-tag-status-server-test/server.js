// #############################################################################
// WEBSOCKET
var os = require("os");
var hostname = os.hostname();
console.log('Hostname:'+hostname);


const WebSocket = require('ws');
var message = {"type":"tag_state","device_id":"525939b49314","disabled":false};


function connectToWebSocket() {
  //ws = new WebSocket('ws://PC-SR1:9898/');
  ws = new WebSocket('ws://172.17.5.8:6789');
  ws.onopen = function() {
    // subscribe to some channels
    console.log('WebSocket opened...')
    ws.send(JSON.stringify(message));
  };

  ws.onmessage = function(e) {
    console.log('Message:', e.data);
  };

  ws.onclose = function(e) {
    console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
    setTimeout(function() {
      connectToWebSocket();
    }, 1000);
  };

  ws.onerror = function(err) {
    console.error('Socket encountered error: ', err.message, 'Closing socket');
    ws.close();
  };
}

connectToWebSocket();


  


// setInterval(function(){
//   if(ws.OPEN){
//     ws.send(JSON.stringify(message));
//   } 
//   if(ws.CLOSED){
//      const ws = new WebSocket('ws://PC-SR1:9898/');
//      ws.addEventListener('error', (err) => console.log(err.message));
//   }
// }, 5000);